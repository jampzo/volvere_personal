<?php
$mensaje = array(
	"tomar un caf&eacute",
	"hacer ejercicios",
	"visitar a la abuela",
	"comer un hotdog",
	"ir a cenar",
	"ir a desayunar",
	"ir a almorzar",
	"hablar por tel&eacutefono",
	"comer un helado",
	"hacer bromas telef&oacutenicas",
	"ver una pel&iacute en netflix",
	"llamar a mi madre",
	"comer un pastel",
	"ir al cine",
	"escuchar lo nuevo en Spotify",
	"ver el Instagram",
	"ver el facebook",
	"enviar foto de mi perro",
	"arreglar mi cuarto",
	"pagar la cuenta de anoche",
	"lavar mi ropa",
	"ir a jap&oacuten",
	"ducharme",
	"buscar a mi perro",
	"ir al veterinario",
	"alegrar a mi novia... me com&iacute su pastel",
	"llevar a mi hermana a la uni",
	"visitar a la tia fastidiosa"
);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Ing. Jesús Mata" />
    <meta name="organization" content="jampzo inc." />
    <meta name="locality" content="Panama, Venezuela, Perú, Chile, Argentina, España" />
    <meta name="keywords" content="Panama, Venezuela, Perú, Chile, Argentina, España, tecnología, sistemas, programación, proyecto" />
    <meta name="description" content="Ingeniero en informática dedicado a facilitar las labores de sus clientes con soluciones integrales y software a la medida." />
    <title>Volver&eacute ;)</title>
    <link rel="shortcut icon" href="/assets/img/favicon.png" />
    <link href="/assets/css/preload.css" rel="stylesheet">
    <link href="/assets/css/vendors.css" rel="stylesheet">
    <link href="/assets/css/syntaxhighlighter/shCore.css" rel="stylesheet">
    <link href="/assets/css/style-blue.css" rel="stylesheet" title="default">
    <link href="/assets/css/width-full.css" rel="stylesheet" title="default">
</head>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<body>
    <div class="coming-back">
        <div class="coming-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="coming-counter">
                            <h1 class="animated fadeInRight animation-delay-4">Volver&eacute!</h1></div>
                    </div>
                </div>
                <div class="row">Luego de <?php echo $mensaje[rand(0, count($mensaje)-1)]; ?>...</div>
            </div>
        </div>
    </div>
    <script src="/assets/js/vendors.js"></script>
    <script src="/assets/js/syntaxhighlighter/shCore.js"></script>
    <script src="/assets/js/syntaxhighlighter/shBrushXml.js"></script>
    <script src="/assets/js/syntaxhighlighter/shBrushJScript.js"></script>
    <script src="/assets/js/app.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121089558-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121089558-1');
</script>


</body>
</html>
